﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Creator : MonoBehaviour
{

    public GameObject Frog;
    public float countdown;
    public bool isGame;
    // Start is called before the first frame update
    void Start()
    {
     
    }

    public void StartCreator() {

        countdown = Random.Range(3, 10);
        Invoke("CreateFrog", countdown);
        isGame = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CreateFrog() {
        if (isGame == true) {
            Instantiate(Frog, transform.position, transform.rotation);
            print(transform.name.ToString());
            //random again
            countdown = Random.Range(3, 10);
            Invoke("CreateFrog", countdown);
        }
    }
}

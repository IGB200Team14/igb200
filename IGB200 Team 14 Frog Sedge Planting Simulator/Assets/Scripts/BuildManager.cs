﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildManager : MonoBehaviour
{

    public static BuildManager instance;

    void Awake() {

        if (instance != null) {
            Debug.LogError("More than one buildmanager in scene");
            return;
        }
        instance = this;
        
    }

    public GameObject standardWSFPrefab;
    public GameObject anotherelementPrefab;
    public GameObject LandElementPrefab;
    public GameObject RockElementPrefab;

    /*void Start() {
        WSF = standardWSFPrefab;
    }*/

    private GameObject WSF;

    public GameObject GetWSFBuild() {
        return WSF;
    }

    public void SetElementBuild(GameObject Element) {

        WSF = Element;

    }


}

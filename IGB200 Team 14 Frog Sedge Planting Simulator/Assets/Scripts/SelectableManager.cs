﻿using UnityEngine;
using UnityEngine.EventSystems;

public class SelectableManager : MonoBehaviour
{
    public Color hovercolor;
    public Vector3 positionOffset;

    private GameObject landfield;

    BuildManager buildManager;
    
    private Renderer rend;
    private Color startColor;


    void Start() {

        rend = GetComponent<Renderer>();
        startColor = rend.material.color;

        buildManager = BuildManager.instance;

    }

    void OnMouseDown() {

        if (EventSystem.current.IsPointerOverGameObject())
            return;

        if (buildManager.GetWSFBuild() == null)
            return;

        if(landfield != null) {
            Debug.Log("Can't Build there! - TODO: Display On Screen");
            return;
        }

        //Build Land
        GameObject WSF = buildManager.GetWSFBuild();
        landfield = (GameObject)Instantiate(WSF, transform.position + positionOffset, transform.rotation);


    }

    void OnMouseEnter () {

        if (EventSystem.current.IsPointerOverGameObject())
            return;

        if (buildManager.GetWSFBuild() == null)
            return;

        rend.material.color = hovercolor;

        
    }

    void OnMouseExit() {

        rend.material.color = startColor; 

    }

}

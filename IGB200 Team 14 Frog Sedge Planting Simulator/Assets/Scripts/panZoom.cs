﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class panZoom : MonoBehaviour
{

    Vector3 touchStart;
    public Camera cam;
    public float groundZ = 0;
    public float minHeight = 10f;
    public float maxHeight = 80f;
    public float zoomOutMin = 1f;
    public float zoomOutMax = 8f;
    public float panSpeed;
    private float panDetect = 15f;


    public float leftBoundary = 20f;
    public float rightBoundary = 20f;
    public float upBoundary = 30f;
    public float lowBoundary = 1f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {


        MoveCamera();

        if (Input.GetMouseButtonDown(0)) {
            touchStart = GetWorldPosition(groundZ);

        }
        if(Input.touchCount == 2) {
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            float prevMagnitude = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float currentMagnitude = (touchZero.position - touchOne.position).magnitude;

            float difference = currentMagnitude - prevMagnitude;

            zoom(difference * 0.01f);

        }
        else if (Input.GetMouseButton(0)) {
            Vector3 direction = touchStart - GetWorldPosition(groundZ);
            Camera.main.transform.position += direction;
            
        }

        zoom(Input.GetAxis("Mouse ScrollWheel"));

        Vector3 pos = transform.position;
        pos.x = Mathf.Clamp(transform.position.x, leftBoundary, rightBoundary);
        pos.y = Mathf.Clamp(transform.position.y, upBoundary, lowBoundary);
    }

    void zoom(float increment) {
        Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize - increment, zoomOutMin, zoomOutMax);
    }

    private Vector3 GetWorldPosition(float z) {
        Ray mousePos = cam.ScreenPointToRay(Input.mousePosition);
        Plane ground = new Plane(Vector3.forward, new Vector3(0, 0, z));
        float distance;
        ground.Raycast(mousePos, out distance);
        return mousePos.GetPoint(distance);
    }

    void MoveCamera() {

        float moveX = Camera.main.transform.position.x;
        float moveY = Camera.main.transform.position.y;
        float moveZ = Camera.main.transform.position.z;

        float xPos = Input.mousePosition.x;
        float yPos = Input.mousePosition.y;
        float zPos = Input.mousePosition.z;

        if (Input.GetKey(KeyCode.A) || xPos > 0 && xPos < panDetect) {

            moveX -= panSpeed;

        } else if (Input.GetKey(KeyCode.D) || xPos < Screen.width && xPos > Screen.width - panDetect) {
            moveX += panSpeed;
        }

        if (Input.GetKey(KeyCode.W) || zPos < Screen.height && zPos > Screen.height - panDetect) {
            moveY += panSpeed;
        } else if (Input.GetKey(KeyCode.S) || zPos > 0 && yPos < panDetect) {
            moveY -= panSpeed;
        }

        moveZ += Input.GetAxis("Mouse ScrollWheel") * (panSpeed * 20);

        moveZ = Mathf.Clamp(moveZ, minHeight, maxHeight);

        moveX = Mathf.Clamp(moveX, leftBoundary, rightBoundary);

        moveY = Mathf.Clamp(moveY, upBoundary, lowBoundary);

        Vector3 newPos = new Vector3(moveX, moveY, moveZ);

        Camera.main.transform.position = newPos;
    }
}

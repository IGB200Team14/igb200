﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{

    public int population;
    public Creator[] Hole;
    public GameObject Canvas;
    public Text time_text;
    public Text score_text;
    public Touch camera_touch;
    public int nowtime;

    // Start is called before the first frame update
    void Start()
    {
        population = 0;
        /*for (int a = 0; a < Hole.Length; a++) {
            Hole[a].enabled = false;
        }*/
        
    }

    public void startGame() {
        for (int a = 0; a < Hole.Length; a++) {
            //Hole[a].enabled = true;
            Hole[a].StartCreator();
        }
        Canvas.SetActive(false);
        nowtime = 60;
        time_text.text = nowtime.ToString(); 
        Invoke("gameCountDown", 1.0f);
        camera_touch.isGame = true;
    }

    public void gameCountDown() {

        nowtime--;
        time_text.text = nowtime.ToString();
        if(nowtime == 0) {
            print("End Game");
            EndGame();
        }else {
            Invoke("gameCountDown", 1.0f);
        }

    }

    public void EndGame() {
        for (int i = 0; i < Hole.Length; i++) {
            Hole[i].isGame = false;
        }
        camera_touch.isGame = false;

    }

    public void GetScore(int getscore) {
        population += getscore;
        score_text.text = population.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

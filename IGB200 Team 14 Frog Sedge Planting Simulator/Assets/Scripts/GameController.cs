﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{

    public GameObject FrogsPrefab;


    public List<Frogs> FrogList = new List<Frogs>();
    public List<Transform> platformList = new List<Transform>();
    public List<Image> heartList = new List<Image>();


    public Text ScoreText;
    public Image timeBar;
    

    public GameObject gameOverCanvas;
    public Text gameOverText;

    private float hp = 3.0f;
    private int score = 0;

    private float gameTime = 60.0f;
    private bool isGameover = false;


    private KeyCode[] keyCodes = {
        KeyCode.Keypad1,
        KeyCode.Keypad2,
        KeyCode.Keypad3,
        KeyCode.Keypad4,
        KeyCode.Keypad5,
        KeyCode.Keypad6,
        KeyCode.Keypad7,
        KeyCode.Keypad8,
        KeyCode.Keypad9
};

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnRandomly());
    }

    // Update is called once per frame
    void Update()
    {

        if(isGameover == true) {
            return;
        }
        MonitorFrogStatus();
        MonitorInput();
        MonitorTimeandHP();
    }

    IEnumerator SpawnRandomly () {
        while(isGameover == false) {
            float time = (float)Random.Range(1f, 5f);
            yield return new WaitForSeconds(time);
            SpawnFrog();
        }
    }

    int GetNumberOfFrog() {
        int sum = 0;

        for (int i = 0; i < FrogList.Capacity; i++) {
            if (FrogList[i] != null && FrogList[i].isAlived == true) {
                sum++;
            }
        }

        return sum;
    }

    void SpawnFrog() {

        if(GetNumberOfFrog() >= 6) {
            return;
        }

        int index = (int)Random.Range(0, 8);
        while(FrogList[index] != null && FrogList[index].isAlived == true) {
            index = (int)Random.Range(0, 8);
        }

        GameObject frog = (GameObject)Instantiate(FrogsPrefab, platformList[index].position, FrogsPrefab.transform.rotation);

        FrogList[index] = frog.GetComponent<Frogs>();
        FrogList[index].isAlived = true;

    }

    void MonitorFrogStatus() {
        for (int i = 0; i < FrogList.Capacity; i++) {
            if(FrogList[i] != null && FrogList[i].transform.position.y >= -2.0f) {
                FrogList[i].isAppearing = true;
            }
            if(FrogList[i] != null && FrogList[i].isAppearing == true && FrogList[i].isAlived == true && FrogList[i].transform.position.y < -2.0f) {
                Destroy(FrogList[i].gameObject);
                FrogList[i].isAppearing = false;
                FrogList[i].isAlived = false;
                FrogList[i] = null;

                deductHP();
            }
        }
    }


    void MonitorInput() {
        if (Input.GetMouseButton(0) /*&& isGame == true*/) {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity)) {
                if (hit.transform.tag == "Frog") {
                    print("Populate Frog");
                    //Script.GetScore(1);
                    addScore();
                    //Destroy(hit.transform.gameObject);
                    MonitorInputFalse();


                }
            }

            

        }
    }

    void MonitorInputFalse() {
        for(int i = 0; i < keyCodes.Length; i++) {
            if(Input.GetKeyDown(keyCodes[i])) {
                if(FrogList[i] != null && FrogList[i].isAppearing == true) {
                    Destroy(FrogList[i].gameObject);
                    FrogList[i].isAppearing = false;
                    FrogList[i].isAlived = false;
                    FrogList[i] = null;
                    addScore();
                }else {
                    deductHP();
                }
            }
        }
    }

    void MonitorTimeandHP() {
        gameTime -= Time.deltaTime;
        timeBar.fillAmount = gameTime / 60.0f;


        if(gameTime <= 0) {
            gameOverCanvas.SetActive(true);
            isGameover = true;
        }
        else if (hp <= 0.0f) {
            gameOverCanvas.SetActive(true);
            isGameover = true;
        }
    }


    void displayScore(int score) {
        ScoreText.text = "X" + score.ToString();
    }

    void DisplayHeart(float hp) {

        //hp 3, index is 2
        int index = (int)Mathf.Floor(hp);
        heartList[index].fillAmount = hp - Mathf.Floor(hp);
    }

    public void addScore() {
        score = score + 1;
        displayScore(score);
    }

    public void deductHP () {
        hp = hp - 1.0f;
        DisplayHeart(hp);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Frogs : MonoBehaviour
{

    public bool isAlived = false;
    public bool isAppearing = false;

    public float timeElapsed = 0.0f;
    public float timeMovedUp = 0.0f;
    public float timeMovedDown = 0.0f;

    private Vector3 oldPosition;
    private Vector3 newPosition;

    // Start is called before the first frame update
    void Start()
    {

        oldPosition = this.transform.position;
        newPosition = new Vector3(oldPosition.x, oldPosition.y + 11.0f, oldPosition.z);

    }

    // Update is called once per frame
    void Update()
    {
        timeElapsed += Time.deltaTime;

        if(timeMovedUp <= 1.0f) {
            timeMovedUp += Time.deltaTime;
            this.transform.position = Vector3.Lerp(oldPosition, newPosition, timeMovedUp);
        }

        if (timeElapsed >= 3 && timeMovedDown <= 1.0f) {
            timeMovedDown += Time.deltaTime;
            this.transform.position = Vector3.Lerp(newPosition, oldPosition, timeMovedDown);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop : MonoBehaviour
{

    BuildManager buildManager;

    void Start() {

        buildManager = BuildManager.instance;
        
    }

    public void purchaseWSF() {

        Debug.Log("WSF selected");
        buildManager.SetElementBuild(buildManager.standardWSFPrefab);

    }

    public void PurchaseOtherElement() {

        Debug.Log("Element Grass Selected");
        buildManager.SetElementBuild(buildManager.anotherelementPrefab);

    }

    public void PurchaseLandElement() {

        Debug.Log("Element Land Selected");
        buildManager.SetElementBuild(buildManager.LandElementPrefab);

    }

    public void PurchaseRockElement() {

        Debug.Log("Element Rock Selected");
        buildManager.SetElementBuild(buildManager.RockElementPrefab);

    }

}

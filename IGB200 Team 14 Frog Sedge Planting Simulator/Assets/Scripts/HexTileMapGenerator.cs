﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexTileMapGenerator : MonoBehaviour
{

    public GameObject hexTilePrefab;

    [SerializeField]int mapWidth = 25;
    [SerializeField]int mapHeight = 12;

    float tileXOffset = 1.555f;
    float tileZOffset = 0.454f;

    // Start is called before the first frame update
    void Start()
    {
        CreateHexTileMap();
    }

    // Update is called once per frame
    void CreateHexTileMap()
    {
        for (int x = 0; x <= mapWidth; x++) {
            for(int z = 0; z <= mapHeight; z++) {
                GameObject TempGo = Instantiate(hexTilePrefab);

                if(z % 2 == 0) {
                    TempGo.transform.position = new Vector3(x * tileXOffset, 0, z * tileZOffset);
                }
                else {
                    TempGo.transform.position = new Vector3(x * tileXOffset + tileXOffset / 2, 0, z * tileZOffset);
                }

                SetTileInfo(TempGo, x, z);
            }
        }
    }

    void SetTileInfo(GameObject Go, int x, int z) {
        Go.transform.parent = transform;

        Go.name = x.ToString() + ", " + z.ToString();
    }
}
